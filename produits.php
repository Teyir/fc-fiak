<?php
require_once "includes/head.php";

title_head("BitGameCoin");
$id=filter_input(INPUT_GET,"id");
$id=filter_input(INPUT_GET,"id");

require_once "config.php";
$pdo = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BDD,Config::UTILISATEUR,Config::MOTDEPASSE);

$requete = $pdo->prepare("select produit.id, titre, description, prixD, imageP, etat_enchere from produit join lots on lots.id=id_lots join encheres on encheres.id=id_encheres where id_lots=:id");
$requete->bindParam(":id",$id);
$requete->execute();

$lignes= $requete->fetchAll();

if (count($lignes)==0){
    echo "pas de produit pour ce lot";
    die;
}

require_once "functions/desc.php";

?>
    <div class="row">
        <?php
        for($i = 0;$i< count($lignes);$i++){
            $description = $lignes[$i]["description"];

            $description = desc($description, 240)

            ?>
            <div class="wrapper">
                <div class="cols">
                    <div class="col" ontouchstart="this.classList.toggle('hover');">
                        <div class="container">
                            <div class="front" style="background-image: url(<?php echo htmlspecialchars($lignes[$i]["imageP"]) ?>)">
                                <div class="inner">
                                    <p><?php echo htmlspecialchars($lignes[$i]["titre"]) ?></p>
                                    <span>Prix de départ :<strong> <?php echo htmlspecialchars($lignes[$i]["prixD"]) ?></strong></span><br>
                                </div>
                            </div>
                            <div class="back">
                                <div class="inner">
                                    <p class="card-text"><?php echo $description ?></p>
                                    <?php if (isset($_SESSION['id_connecté'])==TRUE && $_SESSION['id_connecté']!=0){?>
                                        <?php if (htmlspecialchars($lignes[$i]["etat_enchere"])=="présent"){ ?>
                                            <a href="encherir.php?id=<?php echo htmlspecialchars($lignes[$i]["id"]) ?>" class="btn btn-sm btn-primary"> enchérir </a>
                                        <?php }
                                    }
                                    else {
                                        ?>
                                        <p>Veuillez vous connecter pour accéder à l'enchère</p>
                                        <?php
                                    }
                                    if (isset($_SESSION['admin']) == TRUE){
                                        if ($_SESSION["admin"]==1){ ?>
                                            <a href="admin/modifier_produit.php?id=<?php echo htmlspecialchars($lignes[$i]["id"]) ?>" class="btn btn-sm btn-warning"> modifier </a>
                                        <?php }
                                        if ($_SESSION["admin"]==1){ ?>
                                            <a href="admin/supprimer_produit.php?id=<?php echo htmlspecialchars($lignes[$i]["id"]) ?>" class="btn btn-sm btn-danger"> Supprimer </a>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <?php
        }
        ?>
    </div>

<?php
require_once "includes/footer.php";
