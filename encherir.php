<?php
require_once "includes/head.php";

$token = rand(0, 1000000);

$_SESSION["token"] = $token;

title_head("BitGameCoin");
$id=filter_input(INPUT_GET,"id");

$_SESSION["id_encherir"] = $id;

require_once "config.php";
$pdo = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BDD,Config::UTILISATEUR,Config::MOTDEPASSE);

$requete = $pdo->prepare("select * from produit where id=:id");

$requete->bindParam(":id",$id);
$requete->execute();

$lignes= $requete->fetchAll();

if (count($lignes)!=1){
    http_response_code(404);
    ?>
    <script type="text/javascript">
        window.location.replace("error/oopsi.php");
    </script>
    <?php
    die;
}
$produit=$lignes[0];

$id_produit = $produit["id"];

$requete = $pdo->prepare("SELECT id, montant, id_produit, id_users FROM propositions WHERE id_produit = :id_produit ORDER BY `propositions`.`montant` DESC ");
$requete->bindParam(":id_produit", $id_produit);
$requete->execute();
$donnees = $requete->fetchAll();

$id_user = $donnees[0]["id_users"];


$requete = $pdo->prepare("SELECT dateEXP from produit join lots on produit.id_lots=lots.id join encheres on lots.id_encheres=encheres.id where produit.id =:id");
$requete->bindParam(":id" ,$id);
$requete->execute();

$daterqs = $requete->fetchAll();
$dateexp = $daterqs[0];
$datEXP = $dateexp["dateEXP"];

$requete = $pdo->prepare("SELECT id, montant, id_produit, id_users FROM propositions WHERE id_produit = :id_produit AND id_users = :id_user ORDER BY `propositions`.`montant` DESC ");
$requete->bindParam(":id_produit", $id_produit);
$requete->bindParam(":id_user", $id_user);
$requete->execute();
$data = $requete->fetchAll();

$comptemise = $requete->rowCount();

if ($comptemise >= 1){
    $mise1 = $data[0]["montant"];
} else {
    $mise1 = 0;
}

if ($comptemise >= 2){
    $mise2 = $data[1]["montant"];
} else {
    $mise2 = 0;
}

if ($comptemise >= 3){
    $mise3 = $data[2]["montant"];
} else {
    $mise3 = 0;
}

if ($comptemise >= 4){
    $mise4 = $data[3]["montant"];
} else {
    $mise4 = 0;
}

if ($comptemise >= 5){
    $mise5 = $data[4]["montant"];
} else {
    $mise5 = 0;
}

if (htmlspecialchars($id_user) == $_SESSION["id_connecté"]){
    $result = "enchere_win";

}
else {
    $result = "enchere_loose";

}


?>

    <script>

        setInterval('load_encherir()', 500);

        function load_encherir(){
            $('#resultat').load('load_encherir.php');
        }

    </script>


<?php


$requete = $pdo->prepare('SELECT prixR FROM produit JOIN lots ON produit.id_lots=lots.id JOIN encheres ON lots.id_encheres=encheres.id WHERE produit.id=:id');
$requete->bindParam(":id", $id_produit);
$requete->execute();

$dprixR = $requete->fetchAll();

$montantmin = $dprixR[0]["prixR"];


$last_montant = $donnees[0]["montant"];
$id_user_win = $donnees[0]["id_users"];


$dateEXP = strtotime($datEXP);
$date = strtotime(date("Y-m-d H:i:s"));


if ($dateEXP >= $date )
{

    if ($last_montant <= $montantmin)
    {
        ?>
        <div class="win">
            <p class="text-center">Le joueur <?php echo $id_user_win ?> vient de remporter l'enchère avec un montant de <?php echo $last_montant ?></p>
        </div>
        <?php
    }

}

?>





    <div class="container">

        <div class="total">

            <div class="temp">
                <div class="encherir">

                    <div id="resultat">

                        <p class="montant">Votre position dans l'enchère:

                            <?php if (htmlspecialchars($donnees[0]["id_users"]) == $_SESSION["id_connecté"]) : ?>


                                <i class="fad fa-chart-line fa-8x stonks"></i>
                            <?php else : ?>

                                <i class="fad fa-chart-line-down fa-8x not_stonks"></i>

                            <?php endif; ?>
                        </p>
                    </div>
                    <form method="get" action="actions/encherir.php">
                        <input type="hidden" value="<?php echo $produit["id"] ?>" name="id_produit">
                        <input type="hidden" value="<?php echo $token ?>" name="token" id="token">
                        <input type="hidden" id="pseudo" name="pseudo" value="<?php echo $_SESSION["id_connecté"] ?>" >
                        <div class="input-group">
                            <input type="number" id="mise" name="mise" class="mise form-control" placeholder="ENTREZ VOTRE MISE" required>
                            <button type="submit" class="btn btn-primary bcom">Envoyer</button>
                        </div>
                        <p class="prixDepart">Prix de Départ : <strong><?php echo htmlspecialchars($produit["prixD"])?></strong></p>
                    </form>
                </div>
                <div >

                    <canvas id="myChart"></canvas>

                    <script>
                        var montant1 = <?php echo json_encode($mise1); ?>;
                        var montant2 = <?php echo json_encode($mise2); ?>;
                        var montant3 = <?php echo json_encode($mise3); ?>;
                        var montant4 = <?php echo json_encode($mise4); ?>;
                        var montant5 = <?php echo json_encode($mise5); ?>;

                        var ctx = document.getElementById('myChart').getContext('2d');
                        var chart = new Chart(ctx, {

                            type: 'line',


                            data: {
                                labels: ['1', '2', '3', '4', '5'],
                                datasets: [{
                                    label: 'Suivis de vos 5 dernières mises',
                                    backgroundColor: 'rgb(85,163,214)',
                                    borderColor: 'rgb(21,110,169)',
                                    data: [montant5, montant4, montant3, montant2, montant1]
                                }]
                            },

                            // Configuration options go here
                            options: {}
                        });
                    </script>

                </div>
            </div>


            <div class="produit container">

                <div id="carouselExampleIndicators" class="carousel slide car" data-ride="carousel">

                    <div class="carousel-inner">


                        <div class="carousel-item active">
                            <img class="d-block w-100 imgcar" src="<?php echo htmlspecialchars($produit["imageP"])?>" alt="Image principal">
                        </div>
                        <?php


                        $img[1] = $img1 = htmlspecialchars($produit["image1"]);
                        $img[2] = $img2 = htmlspecialchars($produit["image2"]);
                        $img[3] = $img3 = htmlspecialchars($produit["image3"]);
                        $img[4] = $img4 = htmlspecialchars($produit["image4"]);
                        $img[5] = $img5 = htmlspecialchars($produit["image5"]);
                        $img[6] = $img6 = htmlspecialchars($produit["image6"]);
                        $img[7] = $img7 = htmlspecialchars($produit["image7"]);
                        $img[8] = $img8 = htmlspecialchars($produit["image8"]);
                        $img[9] = $img9 = htmlspecialchars($produit["image9"]);

                        $max = 9;
                        $num = 1;

                        while ($num <= $max){
                            if($img[$num] == NULL)
                            {
                                break;
                            }else{
                                ?>

                                <div class="carousel-item ">
                                    <img class="d-block w-100 imgcar" src="<?php echo $img[$num]?>" alt="Image <?php echo $num ?>">
                                </div>

                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Précédent</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Suivant</span>
                                </a>

                                <?php
                                $num++;
                            }
                        }
                        ?>
                    </div>





                    <div class="infos">
                        <h1 class="titre">Enchère : <strong><?php echo htmlspecialchars($produit["titre"])?></strong></h1>
                        <p class="description">Description du Produit : <strong><?php echo $produit["description"]?></strong></p>
                        <p class="dateEXP">L'enchère expire le : <strong><?php echo htmlspecialchars($datEXP)?></strong></p>
                    </div>
                </div>


            </div>

        </div>
    </div>
<?php
$date = date('Y-m-d H:i:s');

if ($datEXP >= $date){
    $requete = $pdo->prepare("UPDATE encheres SET etat_enchere = REPLACE(etat_enchere, 'futur', 'passé') where id = :id");
    $requete->bindParam(":id", $idenchere);
    $requete->execute();
}


?>

    <script src="node_modules/chart.js/dist/Chart.js"></script>
    <script>
        var myChart = new Chart(ctx, {...});
    </script>


<?php
require_once "includes/footer.php";
