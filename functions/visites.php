<?php
require_once "config.php";
$pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);


// Gestion des pages vues
if(file_exists('compteur_pages_vues.txt'))
{
    $compteur_f = fopen('compteur_pages_vues.txt', 'r+');
    $compte = fgets($compteur_f);
}
else
{
    $compteur_f = fopen('compteur_pages_vues.txt', 'a+');
    $compte = 0;
}
$compte++;
fseek($compteur_f, 0);
fputs($compteur_f, $compte);
fclose($compteur_f);