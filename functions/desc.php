<?php


// FONCTION DESCRIPTION MAX
$max = 240;

function desc($description, $max)     {
    if (strlen($description) >= $max) {
        $description  = trim(substr($description, 0, $max));
        $description .= '...';
    }
    return $description;
}
