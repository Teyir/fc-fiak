<?php
require_once "../includes/head_admin.php";
require_once "../includes/navbar_admin.php";

$token=rand(0, 1000000);
$_SESSION["token"]=$token;



require_once "../config.php";
$pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

title_head("Administration Créer une enchère | FC-Fiak");
?>

    <h1>Créer une enchère</h1>
    <form action="../actions/créer_enchère_action.php" method="post">
        <input type="hidden" value="<?php echo $token ?>" name="token" id="token">

        <div class="form-group col-md-6">
            <label for="titre_en">Titre de l'enchère <em class="required">*</em></label>
            <input maxlength="50" type="text" id="titre_en" name="titre_en" class="form-control" required>
        </div>

        <div class="form-group col-md-6">
            <label for="dateDEB">Date de début <em class="required">*</em></label>
            <input type="datetime-local" id="datePicker" name="dateDEB" class="form-control" value="<?php echo date('d/m/Y h:m'); ?>" required>
        </div>

        <div class="form-group col-md-6">
            <label for="dateEXP">Date d'expiration <em class="required">*</em></label>
            <input type="datetime-local" id="dateEXP" name="dateEXP" class="form-control" required>
        </div>

        <div class="form-group col-md-6">
            <label for="description_en">Description</label>
            <textarea name="description_en"  id="editor"></textarea>

            <input type="submit" class="btn btn-primary">
        </div>

    </form>




<?php
require_once "../includes/footer_admin.php";
?>

