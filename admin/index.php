<?php
require_once "../includes/head_admin.php";
if ($_SESSION["admin"]==0){
    echo "oopsie.gif";
    die;
}

require_once "../includes/navbar_admin.php";
//require_once "../functions/visites.php";
require_once "../functions/membres.php";
require_once "../functions/encheres.php";
require_once "../functions/enchères_terminées.php";
require_once "../functions/mises_total.php";
title_head("Administration | FC-Fiak");

?>

<!-- STATS -->


<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="card-counter inscrits">
                <i class="fa fa-users"></i>
                <span class="count-numbers count"><?php echo $nb_membres ?></span>
                <span class="count-name">Membres inscrits</span>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card-counter visites">
                <i class="fas fa-globe"></i>
                <span class="count-numbers "><?php echo number_format($mise) ?></span>
                <span class="count-name">Mises totales</span>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card-counter actives">
                <i class="fa fa-ticket"></i>
                <span class="count-numbers "><?php echo $nb_encheres_on ?></span>
                <span class="count-name">Enchères actives</span>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card-counter finish">
                <i class="fas fa-store-alt-slash"></i>
                <span class="count-numbers "><?php echo $nb_encheres_finish ?></span>
                <span class="count-name">Enchères terminées</span>
            </div>
        </div>
    </div>
</div>



<?php
require_once "../includes/footer_admin.php";
?>


