<?php
require_once "../includes/head_admin.php";
require_once "../includes/navbar_admin.php";
require_once "../config.php";
require_once "../functions/membres.php";


title_head("Administration membres | FC-Fiak");

$max_val = filter_input(INPUT_GET,"max");
?>

<div class="d-flex flex-row">
    <div class="input-group max_val">
        <form method="get">
            <label for="max">Entrez le nombre de membres maximum à afficher</label>
            <input class="form-control" type="number" id="max" name="max" value="<?php echo $max_val ?>" placeholder="Exemple : 10">
            <button type="submit" id="btnMaxVal" class="btn btn-sm btn-success form-control">Valider</button>
        </form>
    </div>

<?php

if ($max_val == 0){
    $max_val = 10;
}else{

    $max_val = filter_input(INPUT_GET,"max");
}

?>

</div>




    <table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">Utilisateur</th>
        <th scope="col">E-mail</th>
        <th scope="col">Date de création</th>
        <th scope="col">Groupe</th>
        <th scope="col">Actions</th>
    </tr>
    </thead>
<?php

// Lister les membres


$liste = $pdo->query('SELECT id, pseudo, email, date_INS, groupe FROM `users` ORDER BY `id` ASC');


$i = 0;
$counter = 0;
$max = $max_val;

while (($donnees = $liste->fetch()) and ($counter < $max))
{
    $pseudo = $donnees['pseudo'];
    $email = $donnees['email'];
    $inscription = $donnees['date_INS'];
    $groupe = $donnees['groupe'];

    $counter++;
?>


    <tbody>
    <tr>
        <th scope="row" class="membres_pseudo"><?php echo $pseudo?></th>
        <td><?php echo $email?></td>
        <td><?php echo $inscription?></td>
        <td><?php echo $groupe?></td>

        <td><a href="modifier_membres.php?id=<?php echo htmlspecialchars($donnees["id"]) ?>" class="btn btn-sm btn-warning">Modifier</a>
        <a href="supprimer_membres.php?id=<?php echo htmlspecialchars($donnees["id"]) ?>" class="btn btn-sm btn-danger">Supprimer</a></td>
    </tr>

<?php }
$liste->closeCursor();

?>


    </tbody>
</table>




<?php
require_once "../includes/footer_admin.php";
?>

