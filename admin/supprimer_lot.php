<?php
require_once "require_del.php";
require_once "../includes/head_admin.php";
require_once "../includes/navbar_admin.php";

$token=rand(0, 1000000);
$_SESSION["token"]=$token;

require_once "../config.php";

$id=filter_input(INPUT_GET,"id");

$pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $pdo->prepare("select titre_lots FROM lots where id=:id");
$requete->bindParam(":id",$id);
$requete->execute();


$donnees = $requete->fetchAll();

if(count($donnees)!=1){
    http_response_code(404);
    ?>
    <script type="text/javascript">
        window.location.replace("error/oopsi.php");
    </script>
    <?php
    die;
}

$enchere=$donnees[0];

?>

<h1>Voulez vous vraiment supprimer ce lot <i><?php echo htmlspecialchars($enchere['titre_lots']) ?></i> ?</h1>
<form action="../actions/supprimer_lot_action.php" method="post">

    <input type="hidden" value="<?php echo $id ?>" name="id">

    <input type="hidden" value="<?php echo $token ?>" name="token">

    <input type="submit" value="supprimer" class="btn btn-danger">

















    <?php
    require_once "../includes/footer_admin.php";
    ?>

