<?php

$token=rand(0, 1000000);
$_SESSION["token"]=$token;

require_once "../includes/head_admin.php";
require_once "../includes/navbar_admin.php";
require_once "../config.php";

title_head("Administration Historique des ventes | FC-Fiak");
require_once "../config.php";
$pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);
$requete = $pdo->prepare("select titre_en, description_en, dateEXP, etat_enchere from encheres");
$requete->execute();
$lignes = $requete-> fetchAll();

?>
<!-- Pourquoi pas rajouter une search bar -->

<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">Titre</th>
        <th scope="col">Description</th>
        <th scope="col">Date d'expiration</th>
        <th scope="col">État enchère</th>
    </tr>
    </thead>
    <?php

    // Lister les enchères

    $liste = $pdo->prepare("select titre_en, description_en, dateEXP, etat_enchere from encheres");
    $liste->execute();

    $i = 0;
    $counter = 0;
    $max = 10;

    while (($donnees = $liste->fetch()) and ($counter < $max))
    {
    $titre = $donnees['titre_en'];
    $description = $donnees['description_en'];
    $dateEXP = $donnees['dateEXP'];
    $etat = $donnees['etat_enchere'];
    if($etat =='passé'){

    $counter++;
    ?>


    <tbody>
    <tr>
        <th scope="row" class="membres_pseudo"><?php echo $titre?></th>
        <td><?php echo $description?></td>
        <td><?php echo $dateEXP?></td>
        <td><?php echo $etat?></td>



    <?php } }
    $liste->closeCursor();

    ?>


    </tbody>
</table>



<?php
require_once "../includes/footer_admin.php";
?>

















