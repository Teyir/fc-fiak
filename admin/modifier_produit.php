<?php
require_once "../includes/head_admin.php";
require_once "../includes/navbar_admin.php";
require_once "../config.php";
title_head("Modification du produit | FC-Fiak");

$idproduit=filter_input(INPUT_GET,"id");

$pdo = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BDD,Config::UTILISATEUR,Config::MOTDEPASSE);

$requete = $pdo->prepare("SELECT id, titre, description, prixD, prixR, nom_proprio, prenom_proprio, contact_proprio, imageP, image1, image2,image3, image4, image5, image6, image7, image8, image9, id_lots, id_gagnant FROM produit where id=:id");
$requete->bindParam(":id",$idproduit);
$requete->execute();

$donnees= $requete->fetchAll();

if (count($donnees)==0){
    http_response_code(404);
    ?>
    <script type="text/javascript">
        window.location.replace("error/oopsi.php");
    </script>
    <?php
    die;
}
$ligne=$donnees[0];

$token=rand(0,100000);
$_SESSION["token"]=$token;
?>
<h1>Modifier un produit</h1>
<form action="../actions/modifier_produit_action.php" method="post">
    <input type="hidden" value="<?php echo $idproduit ?>" name="id" id="id">
    <input type="hidden" value="<?php echo $token ?>" name="token" id="token">
    <div class="form-group col-md-6">
        <label for="titre">Titre du produit <em class="required">*</em></label>
        <input maxlength="50" type="text" value="<?php echo $ligne["titre"]?>" id="titre" name="titre" class="form-control" required>
    </div>

    <div class="form-group col-md-6">
        <label for="prixD">Prix de départ <em class="required">*</em></label>
        <input maxlength="11" type="number" value="<?php echo $ligne["prixD"]?>" id="prixD" name="prixD" class="form-control" required>

        <label for="prixR">Prix de réserve <em class="required">*</em></label>
        <input maxlength="11" type="number" value="<?php echo $ligne["prixR"]?>" id="prixR" name="prixR" class="form-control"  required>
    </div>

    <div class="form-group col-md-6">
        <label for="nom_proprio">Nom du propriétaire <em class="required">*</em></label>
        <input maxlength="50" type="text" value="<?php echo $ligne["nom_proprio"] ?>" id="nom_proprio" name="nom_proprio" class="form-control" required>
    </div>

    <div class="form-group col-md-6">
        <label for="prenom_proprio">Prénom du propriétaire <em class="required">*</em></label>
        <input maxlength="50" type="text" value="<?php echo $ligne["prenom_proprio"] ?>" id="prenom_proprio" name="prenom_proprio" class="form-control" required>
    </div>

    <div class="form-group col-md-6">
        <label for="contact_proprio">Contact du propriétaire <em class="required">*</em></label>
        <input maxlength="50" type="text" value="<?php echo $ligne["contact_proprio"] ?>" id="contact_proprio" name="contact_proprio" class="form-control" required>
    </div>

    <div class="form-group col-md-6">
        <label for="imageP">Image principale <em class="required">*</em></label>
        <input maxlength="500" type="url" value="<?php echo $ligne["imageP"]?>" id="imageP" name="imageP" required>
    </div>

    <div class="form-group col-md-6">
        <label for="image1">Image complémentaire</label>
        <input maxlength="500" type="url" value="<?php echo $ligne["image1"]?>" id="image1" name="image1">
    </div>
    <div class="form-group col-md-6">
        <label for="image2">Image complémentaire</label>
        <input maxlength="500" type="url" value="<?php echo $ligne["image2"]?>" id="image2" name="image2">
    </div>
    <div class="form-group col-md-6">
        <label for="image3">Image complémentaire</label>
        <input maxlength="500" type="url" value="<?php echo $ligne["image3"]?>" id="image3" name="image3">
    </div>
    <div class="form-group col-md-6">
        <label for="image4">Image complémentaire</label>
        <input maxlength="500" type="url" value="<?php echo $ligne["image4"]?>" id="image4" name="image4">
    </div>
    <div class="form-group col-md-6">
        <label for="image5">Image complémentaire</label>
        <input maxlength="500" type="url" value="<?php echo $ligne["image5"]?>" id="image5" name="image5">
    </div>
    <div class="form-group col-md-6">
        <label for="image6">Image complémentaire</label>
        <input maxlength="500" type="url" value="<?php echo $ligne["image6"]?>" id="image6" name="image6">
    </div>
    <div class="form-group col-md-6">
        <label for="image7">Image complémentaire</label>
        <input maxlength="500" type="url" value="<?php echo $ligne["image7"]?>" id="image7" name="image7">
    </div>
    <div class="form-group col-md-6">
        <label for="image8">Image complémentaire</label>
        <input maxlength="500" type="url" value="<?php echo $ligne["image8"]?>" id="image8" name="image8">
    </div>
    <div class="form-group col-md-6">
        <label for="image9">Image complémentaire</label>
        <input maxlength="500" type="url" value="<?php echo $ligne["image9"]?>" id="image9" name="image9">
    </div>

    <?php require_once "../config.php";
    $pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

    $requete = $pdo->prepare('select id, titre_lots from lots');
    $requete->execute();

    $lignes = $requete->fetchAll(); ?>

    <div class="form_group col-md-6">
        <label for="id_lots">Titre du lot <em class="required">*</em></label>
        <select name="id_lots" class="form-group" id="id_lots" required>
            <option value="<?php echo htmlspecialchars($ligne["id_lots"]) ?>">ancienne valeur</option>
            <?php

            for ($i = 0; $i < count($lignes); $i++){
                echo "<option value=&quot;", htmlspecialchars($lignes[$i]["id"]), "&quot;>", htmlspecialchars($lignes[$i]["titre_lots"]), "</option>";
            }

            ?>
        </select>
    </div>

    <?php require_once "../config.php";
    $pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

    $requete = $pdo->prepare('select id, pseudo from users');
    $requete->execute();

    $lignes = $requete->fetchAll();
    if (htmlspecialchars($ligne["id_gagnant"])==null){
        $id_gagne=-1;
    }
    else{
        $id_gagne=htmlspecialchars($ligne["id_gagnant"]);
    }?>

    <div class="form_group col-md-6">
        <label for="id_gagnant">Gagnant de l'enchère</label>
        <select name="id_gagnant" class="form-group" id="id_gagnant">
            <option value="<?php echo htmlspecialchars($ligne["id_gagnant"]) ?>">ancienne valeur</option>
            <?php

            for ($i = 0; $i < count($lignes); $i++){
                echo "<option value=&quot;", htmlspecialchars($lignes[$i]["id"]), "&quot;>", htmlspecialchars($lignes[$i]["pseudo"]), "</option>";
            }

            ?>
        </select>
    </div>

    <div class="form-group col-md-6">
        <label for="description">Description</label>
        <textarea id="editor" name="description"><?php echo $ligne["description"]?></textarea>
    </div>
    <div class="form-group col-md-6">
        <input type="submit" value="Envoyer" class="btn btn-primary ">
    </div>





</form>


<style>
    footer{
        position: relative;
    }
</style>


<?php
require_once "../includes/footer_admin.php";
?>

