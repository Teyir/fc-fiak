<?php
require_once "../includes/head_admin.php";
require_once "../includes/navbar_admin.php";

$token=rand(0, 1000000);
$_SESSION["token"]=$token;


$pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $pdo->prepare('select id, titre_lots from lots');
$requete->execute();

$lignes = $requete->fetchAll();

title_head("Administration Créer un produit | FC-Fiak");
?>

    <h1>Créer un produit</h1>
    <form action="../actions/créer_produit_action.php" method="post">
        <input type="hidden" value="<?php echo $token ?>" name="token" id="token">
        <div class="form-group col-md-6">
            <label for="titre">Titre du produit <em class="required">*</em></label>
            <input maxlength="50" type="text" id="titre" name="titre" class="form-control" required>
        </div>

        <div class="form-group col-md-6">
            <label for="prixD">Prix de départ <em class="required">*</em></label>
            <input maxlength="11" type="number" id="prixD" name="prixD" class="form-control" required>

            <label for="prixR">Prix de réserve <em class="required">*</em></label>
            <input maxlength="11" type="number" id="prixR" name="prixR" class="form-control"  required>
        </div>

        <div class="form-group col-md-6">
            <label for="nom_proprio">Nom du propriétaire <em class="required">*</em></label>
            <input maxlength="50" type="text" id="nom_proprio" name="nom_proprio" class="form-control" required>
        </div>

        <div class="form-group col-md-6">
            <label for="prenom_proprio">Prénom du propriétaire <em class="required">*</em></label>
            <input maxlength="50" type="text" id="prenom_proprio" name="prenom_proprio" class="form-control" required>
        </div>

        <div class="form-group col-md-6">
            <label for="contact_proprio">Contact du propriétaire <em class="required">*</em></label>
            <input maxlength="50" type="text" id="contact_proprio" name="contact_proprio" class="form-control" required>
        </div>

        <div class="form-group col-md-6">
            <label for="imageP">Image principale <em class="required">*</em></label>
            <input maxlength="500" type="url" id="imageP" name="imageP" required>
        </div>

        <div class="form-group col-md-6">
            <label for="image1">Image complémentaire</label>
            <input maxlength="500" type="url" id="image1" name="image1">
        </div>
        <div class="form-group col-md-6">
            <label for="image2">Image complémentaire</label>
            <input maxlength="500" type="url" id="image2" name="image2">
        </div>
        <div class="form-group col-md-6">
            <label for="image3">Image complémentaire</label>
            <input maxlength="500" type="url" id="image3" name="image3">
        </div>
        <div class="form-group col-md-6">
            <label for="image4">Image complémentaire</label>
            <input maxlength="500" type="url" id="image4" name="image4">
        </div>
        <div class="form-group col-md-6">
            <label for="image5">Image complémentaire</label>
            <input maxlength="500" type="url" id="image5" name="image5">
        </div>
        <div class="form-group col-md-6">
            <label for="image6">Image complémentaire</label>
            <input maxlength="500" type="url" id="image6" name="image6">
        </div>
        <div class="form-group col-md-6">
            <label for="image7">Image complémentaire</label>
            <input maxlength="500" type="url" id="image7" name="image7">
        </div>
        <div class="form-group col-md-6">
            <label for="image8">Image complémentaire</label>
            <input maxlength="500" type="url" id="image8" name="image8">
        </div>
        <div class="form-group col-md-6">
            <label for="image9">Image complémentaire</label>
            <input maxlength="500" type="url" id="image9" name="image9">
        </div>




        <div class="form_group col-md-6">
            <label for="id_lots">Titre du lot <em class="required">*</em></label>
            <select name="id_lots" class="form-group" id="id_lots" required>
                <?php

                for ($i = 0; $i < count($lignes); $i++){
                    echo "<option value=&quot;", htmlspecialchars($lignes[$i]["id"]), "&quot;>", htmlspecialchars($lignes[$i]["titre_lots"]), "</option>";
                }

                ?>
            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="description">Description</label>
            <textarea id="editor" name="description"></textarea>

            <input type="submit" value="Envoyer" class="btn btn-primary ">
        </div>

    </form>


<style>
    footer{
        position: relative;
    }
</style>

<?php
require_once "../includes/footer_admin.php";
?>

