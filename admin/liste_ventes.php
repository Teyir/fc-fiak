<?php
require_once "../includes/head_admin.php";
require_once "../includes/navbar_admin.php";

$token=rand(0, 1000000);
$_SESSION["token"]=$token;


require_once "../config.php";

$pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

title_head("Administration liste des ventes | FC-Fiak");





$max_val = filter_input(INPUT_GET,"max");
?>

<div class="d-flex flex-row">
    <div class="input-group max_val">
        <form method="get">
            <label for="max">Entrez le nombre de ventes maximum à afficher</label>
            <input class="form-control" type="number" id="max" name="max" value="<?php echo $max_val ?>" placeholder="Exemple : 10">
            <button type="submit" id="btnMaxVal" class="btn btn-sm btn-success form-control">Valider</button>
        </form>
    </div>

    <?php

    if ($max_val == 0){
        $max_val = 10;
    }else{

        $max_val = filter_input(INPUT_GET,"max");
    }

    if ($max_val > 10)
    {
        ?>
        <style>
            footer{
                position: relative;
            }
        </style>
    <?php
    }

    ?>

</div>

<table class="table table-hover">
    <thead>
    <tr>
        <th scope="col">Titre</th>
        <th scope="col">Description</th>
        <th scope="col">Prix de départ</th>
        <th scope="col">Prix de réserve</th>
    </tr>
    </thead>
    <?php

    // Lister les enchères

    $liste = $pdo->prepare("select id, titre, prixD, description, prixR  from produit");
    $liste->execute();

    $i = 0;
    $counter = 0;
    $max = $max_val;

    while (($donnees = $liste->fetch()) and ($counter < $max))
    {
    $titre = $donnees['titre'];
    $prixD = $donnees['prixD'];
    $prixR = $donnees['prixR'];
    $description = $donnees['description'];


    $counter++;
    ?>


    <tbody>
    <tr>
        <th scope="row" class="membres_pseudo"><?php echo $titre?></th>
        <td><?php echo $description?></td>
        <td><?php echo $prixD?></td>
        <td><?php echo $prixR?></td>


    </tr>

    <?php }
    $liste->closeCursor();

    ?>


    </tbody>
</table>



<?php
require_once "../includes/footer_admin.php";
?>

