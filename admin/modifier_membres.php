<?php

require_once "../includes/head_admin.php";
require_once "../includes/navbar_admin.php";
require_once "../config.php";
require_once "../functions/membres.php";
title_head("Administration membres | FC-Fiak");

require_once "../config.php";

$id=filter_input(INPUT_GET,"id");

$pdo = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BDD,Config::UTILISATEUR,Config::MOTDEPASSE);

$requete = $pdo->prepare("SELECT id, pseudo, email, groupe, avatar FROM `users` where id=:id");
$requete->bindParam(":id",$id);
$requete->execute();

$donnees= $requete->fetchAll();

if (count($donnees)!=1){
    http_response_code(404);
    ?>
    <script type="text/javascript">
        window.location.replace("error/oopsi.php");
    </script>
    <?php
    die;
}
$membre=$donnees[0];

$token=rand(0,100000);
$_SESSION["token"]=$token;
?>

    <h1>Modifier un membres</h1>
    <form action="../actions/Modifier_membres_action.php" method="post">
        <input type="hidden" value="<?php echo $id ?>" name="id" id="id">
        <input type="hidden" value="<?php echo $token ?>" name="token" id="token">
        <div class="form-group">
            <label for="pseudo">pseudo</label>
            <textarea class="form-control" id="pseudo" name="pseudo"><?php echo $membre["pseudo"]?></textarea>
        </div>
        <div class="form-group">
            <label for="avatar">URL de l'avatar</label>
            <textarea class="form-control" id="avatar" name="avatar"><?php echo $membre["avatar"]?></textarea>
        </div>
        <div class="form-group">
            <label for="email">E-mail</label>
            <textarea class="form-control" name="email" id="email" ><?php echo $membre["email"]?></textarea>
        </div>
        <div class="form-group">
            <label for="groupe">groupe</label>
            <select name="groupe" class="form-control" selected="selected"><?php echo $membre["groupe"]?>
                <option value="admin">admin</option>
                <option value="membre">membre</option>

            </select>
        </div>
        <input type="submit" class="btn btn-primary" value="modifier">
    </form>



<?php
require_once "../includes/footer_admin.php";
?>

