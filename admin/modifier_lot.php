<?php

require_once "../includes/head_admin.php";
require_once "../includes/navbar_admin.php";
require_once "../config.php";
title_head("Modification du lot | FC-Fiak");

$id=filter_input(INPUT_GET,"id");

$pdo = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BDD,Config::UTILISATEUR,Config::MOTDEPASSE);

$requete = $pdo->prepare("SELECT id, titre_lots, description_lots, id_encheres FROM lots where id=:id");
$requete->bindParam(":id",$id);
$requete->execute();

$donnees= $requete->fetchAll();

if (count($donnees)==0){
    http_response_code(404);
    ?>
    <script type="text/javascript">
        window.location.replace("error/oopsi.php");
    </script>
    <?php
    die;
}
$ligne=$donnees[0];

$token=rand(0,100000);
$_SESSION["token"]=$token;
?>
    <h1>Modifier un lot</h1>
    <form action="../actions/modifier_lot_action.php" method="post">
        <input type="hidden" value="<?php echo $token ?>" name="token" id="token">

        <div class="form-group col-md-6">
            <label for="titre_lots">Titre du lot <em class="required">*</em></label>
            <input maxlength="50" type="text" value="<?php echo $ligne["titre_lots"]?>" id="titre_lots" name="titre_lots" class="form-control" placeholder="Mon super titre aguicheur" required>
        </div>

        <?php require_once "../config.php";
        $pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

        $requete = $pdo->prepare('select id, titre_en from encheres');
        $requete->execute();

        $lignes = $requete->fetchAll(); ?>

        <div class="form_group col-md-6">
            <label for="id_encheres">Titre de l'enchère</label>
            <select name="id_encheres" class="form-control" id="id_encheres" required>
                <option value="<?php echo htmlspecialchars($ligne["id_encheres"]) ?>">ancienne valeur</option>
                <?php

                for ($i = 0; $i < count($lignes); $i++){
                    echo "<option value=&quot;", htmlspecialchars($lignes[$i]["id"]), "&quot;>", htmlspecialchars($lignes[$i]["titre_en"]), "</option>";
                }

                ?>
            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="description_lots" >Description</label>
            <textarea id="editor" name="description_lots" placeholder="Décrivez le lot que vous souhaitez mettre en vente"><?php echo $ligne["description_lots"]?></textarea>

            <input type="submit" class="btn btn-primary">
        </div>

    </form>



<?php
require_once "../includes/footer_admin.php";
?>

