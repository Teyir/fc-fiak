<?php

require_once "../includes/head_admin.php";
require_once "../includes/navbar_admin.php";
require_once "../config.php";
title_head("Modification de l'enchère | FC-Fiak");

$id=filter_input(INPUT_GET,"id");

$pdo = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BDD,Config::UTILISATEUR,Config::MOTDEPASSE);

$requete = $pdo->prepare("SELECT id, titre_en, description_en, dateDEB, dateEXP, etat_enchere FROM encheres where id=:id");
$requete->bindParam(":id",$id);
$requete->execute();

$donnees= $requete->fetchAll();

if (count($donnees)!=1){
    http_response_code(404);
    ?>
    <script type="text/javascript">
        window.location.replace("error/oopsi.php");
    </script>
    <?php
    die;
}
$ligne=$donnees[0];

$token=rand(0,100000);
$_SESSION["token"]=$token;
?>
<h1>Modifier une enchère</h1>
<form action="../actions/modifier_enchere_action.php" method="post">
    <input type="hidden" value="<?php echo $id ?>" name="id" id="id">
    <input type="hidden" value="<?php echo $token ?>" name="token" id="token">

    <div class="form-group col-md-6">
        <label for="titre_en">Titre de l'enchère <em class="required">*</em></label>
        <input maxlength="50" type="text" value="<?php echo $ligne["titre_en"]?>" id="titre_en" name="titre_en" class="form-control" required>
    </div>

    <div class="form-group col-md-6">
        <label for="dateDEB">Date de début <em class="required">*</em></label>
        <input type="datetime-local" value="<?php echo $ligne["dateDEB"]?>" id="datePicker" name="dateDEB" class="form-control" value="<?php echo date('d/m/Y h:m'); ?>" required>
    </div>

    <div class="form-group col-md-6">
        <label for="dateEXP">Date d'expiration <em class="required">*</em></label>
        <input type="datetime-local" value="<?php echo $ligne["dateEXP"]?>" id="dateEXP" name="dateEXP" class="form-control" required>
    </div>

    <div class="form-group col-md-6">
        <label for="etat_enchere">État de l'enchère<em class="required">*</em></label>
        <input type="text" value="<?php echo $ligne["etat_enchere"]?>" id="etat_enchere" name="etat_enchere" class="form-control" required>
    </div>

    <div class="form-group col-md-6">
        <label for="description_en">Description</label>
        <textarea name="description_en"  id="editor"><?php echo $ligne["description_en"]?></textarea>

        <input type="submit" class="btn btn-primary">
    </div>

</form>



<?php
require_once "../includes/footer_admin.php";
?>


