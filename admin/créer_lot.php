<?php
require_once "../includes/head_admin.php";
require_once "../includes/navbar_admin.php";

$token=rand(0, 1000000);
$_SESSION["token"]=$token;



require_once "../config.php";
$pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $pdo->prepare('select id, titre_en from encheres');
$requete->execute();

$lignes = $requete->fetchAll();

title_head("Administration Créer un lot | FC-Fiak");
?>

    <h1>Créer un lot</h1>
    <form action="../actions/créer_lot_action.php" method="post">
        <input type="hidden" value="<?php echo $token ?>" name="token" id="token">

        <div class="form-group col-md-6">
            <label for="titre_lots">Titre du lot <em class="required">*</em></label>
            <input maxlength="50" type="text" id="titre_lots" name="titre_lots" class="form-control" placeholder="Mon super titre aguicheur" required>
        </div>

        <div class="form_group col-md-6">
            <label for="id_encheres">Titre de l'enchère</label>
            <select name="id_encheres" class="form-control" id="id_encheres" required>
                <?php

                for ($i = 0; $i < count($lignes); $i++){
                    echo "<option value=&quot;", htmlspecialchars($lignes[$i]["id"]), "&quot;>", htmlspecialchars($lignes[$i]["titre_en"]), "</option>";
                }

                ?>
            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="description_lots" >Description</label>
            <textarea id="editor" name="description_lots" placeholder="Décrivez le lot que vous souhaitez mettre en vente"></textarea>

            <input type="submit" class="btn btn-primary">
        </div>


    </form>



<?php
require_once "../includes/footer_admin.php";
?>


