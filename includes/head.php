<?php
header('Content-type: text/html; charset=utf-8');
date_default_timezone_set('Europe/Paris');
function title_head($title){
    ?>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <!-- SEO -->
        <meta name="author" content="FC-Fiak">
        <meta name="publisher" content="fc-fiak.fr [contact@fc-fiak.fr]"/>
        <meta name="description" content="Découvrez la premier site de ventes aux enchères en ligne à l'aveugle !">
        <meta name="keywords" content="fc-fiak, fcfiak, bidgamecoin, bid game coin, ventes aux enchères, vente, ventes, enchere, encheres, enchères">
        <!-- Quand le site sera en ligne faudra penser à mettre le logo (le liens)-->
        <meta property="og:image" content="">
        <meta property="og:description" content="Découvrez la premier site de ventes aux enchères en ligne à l'aveugle !">
        <meta property="og:title" content="Logo FC-Fiak.fr">
        <meta name="copyright" content="FC-Fiak" />
        <!-- Gestion du bot -->
        <meta name="robots" content="follow, index, all">
        <meta name="google" content="notranslate">
        <meta property="og:type" content="Site">
        <meta property="og:site_name" content="FC-Fiak"/>
        <meta property="og:url" content="https://fc-fiak.fr"/>



        <title><?php echo $title ?></title>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

        <script src="https://kit.fontawesome.com/edc8d5fc95.js" crossorigin="anonymous"></script>

        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <link type="text/css" rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" type="text/css" href="./css/encheres.css">
        <link rel="stylesheet" type="text/css" href="./css/encherir.css">
        <link rel="stylesheet" type="text/css" href="../css/accueil.css">
        <link rel="stylesheet" type="text/css" href="css/participer-enchere.css">
        <link rel="stylesheet" type="text/css" href="css/profil.css">
        <link rel="stylesheet" type="text/css" href="./css/style.css">


        <script src="https://cdn.tiny.cloud/1/v35iwewi2aps87b8wegyv46ynxk3rlvjzzv4xspz578ufd3c/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
        <script type="text/javascript" src="/js/jquery.js"></script>
        <script type="text/javascript" src="js/javascript.js"></script>

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <!-- Polices -->
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet"/>


        <!-- Éditeur de texte -->
        <script src="https://cdn.ckeditor.com/ckeditor5/24.0.0/classic/ckeditor.js"></script>


        <!-- Favicon -->

        <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
        <link rel="manifest" href="/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <!-- Chart.Js -->
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>





    </head>
    <?php
}

require_once "entete.php";

require_once "config.php";
$pdo = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BDD,Config::UTILISATEUR,Config::MOTDEPASSE);
$requete = $pdo->prepare("select id, dateDEB, dateEXP, process_gagne from encheres");
$requete->execute();
$lignes = $requete-> fetchAll();

for ($i=0;$i<count($lignes);$i++){
    $dateEXP=$lignes[$i]['dateEXP'];
    $dateDEB=$lignes[$i]['dateDEB'];

    $expiration = strtotime($dateEXP);
    $debut = strtotime($dateDEB);
    $maintenant = strtotime(date("Y-m-d H:i:s"));
    if ($expiration<=$maintenant){
        $etat_enchere = "passé";
    }
    elseif ($debut<=$maintenant){
        $etat_enchere = "présent";
    }
    elseif ($debut>=$maintenant){
        $etat_enchere = "futur";
    }
    else{
        $etat_enchere = "error";
    }

    $id=$lignes[$i]['id'];

    $requete = $pdo->prepare("update encheres set etat_enchere=:etat_enchere where id=:id");
    $requete->bindParam(":etat_enchere", $etat_enchere);
    $requete->bindParam(":id", $id);
    $requete->execute();

    $process_gagne=htmlspecialchars($lignes[$i]['process_gagne']);
    if ($process_gagne==FALSE && $etat_enchere=="passé"){
        $requete = $pdo->prepare("select produit.id from produit join lots on id_lots=lots.id join encheres on id_encheres=encheres.id where encheres.id=:id");
        $requete->bindParam(":id", $id);
        $requete->execute();
        $gagne = $requete-> fetchAll();
        for ($j=0;$j<count($gagne);$j++){
            $id_prod=$gagne[$j]['id'];
            $requete = $pdo->prepare("select montant, id_users from propositions where id_produit=:id order by montant desc");
            $requete->bindParam(":id", $id_prod);
            $requete->execute();
            $gagnant=$requete->fetchAll();

            if (count($gagnant)>0){
                $id_gagnant=$gagnant[0]['id_users'];

                $requete = $pdo->prepare("update produit set id_gagnant=:id_gagnant where id=:id");
                $requete->bindParam(":id", $id_prod);
                $requete->bindParam(":id_gagnant", $id_gagnant);
                $requete->execute();
                unset($gagnant);
                unset($id_gagnant);
            }
        }
        $requete = $pdo->prepare("update encheres set process_gagne=TRUE where id=:id");
        $requete->bindParam(":id", $id);
        $requete->execute();
    }
}
?>
