<?php

$_SESSION["id_connecté"];

$id = $_SESSION["id_connecté"];

$pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $pdo->prepare("SELECT pseudo FROM users WHERE (id=:id)");
$requete->bindParam(":id", $id);

$requete->execute();

$lignes = $requete->fetchAll();

$pseudo = htmlspecialchars($lignes[0]["pseudo"]);
?>
<!-- NAVBAR -->

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="../index.php" >FC-Fiak</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">

    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="membres.php">Membres</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Ventes
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="../admin/créer_enchère.php">Créer une enchère</a>
                    <a class="dropdown-item" href="../admin/créer_lot.php">Créer un lot</a>
                    <a class="dropdown-item" href="../admin/créer_produit.php">Créer un produit</a>
                    <a class="dropdown-item" href="../admin/liste_ventes.php">Liste des ventes</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="../admin/historique_ventes.php">Historique des ventes</a>
                </div>
            </li>
        </ul>

        <form class="form-inline my-2 my-lg-0">
            <span class="navbar-brand pseudo"><?php echo $pseudo ?></span>

            <a class="navbar-brand" href="../actions/deconnexion_action.php"> <i class="fas fa-power-off"></i> Se déconnecter</a>
        </form>

    </div>
</nav>
