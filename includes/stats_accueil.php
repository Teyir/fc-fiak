<?php

require_once "config.php";

$pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);


// COMPTER le nombre d'enchères actives
$query = $pdo->prepare('SELECT * FROM encheres');
$query->execute();
$array = $query->fetchAll();
$nb_encheres_on = count($array);



// Compter le nombre d'enchères terminées
$countfinish = $pdo->prepare('select etat_enchere from encheres where etat_enchere = "passé"');
$countfinish->execute();
$array = $countfinish->fetchAll();
$nb_encheres_finish = count($array);



// COMPTER le nombre de membres
$count = $pdo->prepare('SELECT * FROM users');
$count->execute();
$array = $count->fetchAll();
$nb_membres = count($array);


// COMPTER le nombre de produits en vente
$count123 = $pdo->prepare('SELECT id FROM `produit`');
$count123->execute();
$array = $count123->fetchAll();
$nb_produits = count($array);

