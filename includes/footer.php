<?php
require_once "functions/visites.php";
    $pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

// Afficher les 3 derniers membres inscrits

$count = $pdo->query('SELECT id, pseudo, date_INS FROM `users` ORDER BY `users`.`date_INS` DESC');

$counter = 0;
$max = 3;

?>


    <section id="footer">
        <div class="container">
            <div class="row text-center text-xs-center text-sm-left text-md-left">
                <div class="col-xs-12 col-sm-4 col-md-4 footerglob">
                    <h5>Liens utiles</h5>
                    <ul class="list-unstyled quick-links">
                        <li><a href="index.php"><i class="fa fa-angle-double-right"></i>Accueil</a></li>
                        <li><a href="informations.php"><i class="fa fa-angle-double-right"></i>Informations</a></li>
                        <li><a href="encheres.php"><i class="fa fa-angle-double-right"></i>Enchères en cours</a></li>
                        <li><a href="cgv.php"><i class="fa fa-angle-double-right"></i>CGV</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 footerglob">
                    <h5>3 derniers membres inscrits:</h5>
                    <?php while (($donnees = $count->fetch()) and ($counter < $max)) { ?>

                        <ul class="list-unstyled footer_membres">
                            <li class="footer_txt">Pseudo: <strong class="footer_strong"><?php echo $donnees['pseudo']?></strong></li>
                            <li class="footer_txt">Membre n°  <strong class="footer_strong"><?php echo $donnees['id']?></strong></li>
                            <li class="footer_txt">Inscrit le:  <strong class="footer_strong"><?php echo $donnees['date_INS']?></strong></li>
                        </ul>


                    <?php $counter++; ?>
                    <?php }
                    $count->closeCursor();?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center text-white">
                    <p class="h6">© Tous droits réservés | 2020 - 2021 | FC-Fiak.fr</p>
                </div>
                <hr>
            </div>
        </div>
    </section>




    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
    <script>
        AOS.init();
    </script>
    </div>
    </body>

    </html>
<?php// Gestion des pages vues
if(file_exists('compteur_pages_vues.txt'))
{
    $compteur_f = fopen('compteur_pages_vues.txt', 'r+');
    $compte = fgets($compteur_f);
}
else
{
    $compteur_f = fopen('compteur_pages_vues.txt', 'a+');
    $compte = 0;
}
$compte++;
fseek($compteur_f, 0);
fputs($compteur_f, $compte);
fclose($compteur_f);



