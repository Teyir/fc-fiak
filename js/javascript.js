
// Compteur
$(document).ready(function(){

    $('.statistic-counter').counterUp({
        delay: 10,
        time: 2000
    });

});




// Mettre par défaut le date + heure
$.fn.setNow = function (onlyBlank) {
    var now = new Date($.now())
        , année
        , mois
        , jour
        , heures
        , minutes
        , formattedDateTime
    ;

    année = now.getFullYear();
    mois = now.getMonth().toString().length === 1 ? '0' + (now.getMonth() + 1).toString() : now.getMonth() + 1;
    jour = now.getDate().toString().length === 1 ? '0' + (now.getDate()).toString() : now.getDate();
    heures = now.getHours().toString().length === 1 ? '0' + now.getHours().toString() : now.getHours();
    minutes = now.getMinutes().toString().length === 1 ? '0' + now.getMinutes().toString() : now.getMinutes();

    formattedDateTime = année + '-' + mois + '-' + jour + 'T' + heures + ':' + minutes;

    if ( onlyBlank === true && $(this).val() ) {
        return this;
    }

    $(this).val(formattedDateTime);

    return this;
}

$(function () {
    $('input[type="datetime-local"]').setNow();
});