
<html>
<head>
    <title></title>

    <!--jQuery-->
    <script src = "https://code.jquery.com/jquery-3.1.1.js"></script>
    <link rel="stylesheet" type="text/css" href="oopsi.css">
</head>

<body>
<!--to catch new dive above loading-->
<div id="containerTop">
</div>

<canvas id="myCanvas"></canvas>
<div id="txt">Merci de patienter</div>
<canvas id="myConsole"></canvas>



<script>
    /*====== JS ======*/

    window.onload = function(){

        /*====== Canvas wait ======*/

        var canvas = document.getElementById('myCanvas');
        var context = canvas.getContext('2d');
        canvas.width = 200;
        canvas.height = 100;

        context.strokeStyle = "lightgrey";
        context.lineWidth = 8;

        setInterval(waitIcon, 1000/30);

        /*-- var x used for context.arc() starting point --*/
        var x = 0;
        /*-- var y used for context.arc() ending point --*/
        var y = 0;

        function waitIcon(){

            context.clearRect(0, 0, canvas.width, canvas.height);

            context.beginPath(); context.arc(100,50,40,x,Math.PI*2-y);
            context.stroke();
            context.closePath();

            x+=0.15;
            y-=0.05;

        }



        var canvasConsole = document.getElementById('myConsole');
        var consoleContext = canvasConsole.getContext('2d');

        canvasConsole.width = window.innerWidth;

        consoleContext.fillStyle = "white";

        setInterval(consoleAnim, 1000/30)

        /* Effet console */
        var X = 0;

        function consoleAnim(){

            consoleContext.clearRect(0, 0, canvas.width, canvas.height)

            consoleContext.beginPath();
            consoleContext.fillRect(X,5, 5, 15);
            consoleContext.closePath();

            consoleContext.fillText("$run:/ezez.exe/",15,15);

            X+= 0.25;
        }
    }

    /* jQuery */

    $(document).ready(function(){

        $('#myConsole').hide()

        setTimeout(function(){
            $(document.body).append('<div id="new">Attendez quelques instants ....</br></div>')
            $('#new').css({
                backgroundColor: 'lightgrey',
                borderRadius: '3px',
                textAlign: 'center',
                width: '50%',
                margin:'auto',
                marginTop: '5px',
            })
            $('#new').hide()
            $('#new').show('slow')
        }, 1500)

        setTimeout(function(){
            $('#containerTop').append('<div id="newTop">En route vers un site incroyable</div>')
            $('#newTop').css({
                textAlign: 'center',
                backgroundColor: 'lightgrey',
                bordeRadius: '3px',
                width: '100%',
            })
            $('#newTop').hide()
            $('#newTop').show('slow')
        }, 2000)

        setTimeout(function(){
            $(document.body).append('<div id=side>Log</div>')
            $('#side').css({
                position: 'absolute',
                left: '0',
                top: '50px',
            })
            $('#side').hide()
            $('#side').show('slow')
        }, 2500)

        setTimeout(function(){
            $('#newTop').css({
                width: '80%',
                margin: 'auto',
            })
        }, 3000)

        setTimeout(function(){
            $('#myConsole').css({
                backgroundColor: 'black',
                color: 'white',
                display: 'block',
                margin: 'auto',
                marginTop: '5px'
            })
            $('#side').remove()
        }, 3700)

        setTimeout(function(){
            $('#myCanvas').remove()
            $('#txt').html('Ready')
            $('#txt').css({
                color: 'Green',
                animationName:'txtReady',
                animationDuration:'2s',
                animationDirection: 'alternate',
                animationIterationCount: 'infinite',
                animationTimingFunction: 'linear',
            })
        }, 4700)

        setTimeout(function(){
            $('#txt').remove()
        }, 5700)

        setTimeout(function(){
            $('#newTop').remove()
        }, 5900)

        setTimeout(function(){
            $('#myConsole').remove()
            $('#new').remove()
            $('body, html').css({
                backgroundColor: 'black',
            })
            $('body').append('<div id="virus">Un peux de divertissement dans ce monde de brutes</div>')
            $('#virus').css({
                color: 'chartreuse',
                fontSize: '2em',
            })
            $('body').append('<video width="500" height="300" controls autoplay>\n' +
                             '  <source src="oopsi.mp4" type="video/mp4">\n' +
                             '</video>')
            $('body').append('<p style="color:chartreuse"> Bonne visite sur notre jolie site (:</p>')
        }, 11000)
    })
</script>

</body>
</html>
