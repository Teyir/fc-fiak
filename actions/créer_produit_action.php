<?php
session_start();

$titre = filter_input(INPUT_POST, "titre");
$description = filter_input(INPUT_POST, "description");
$prixD = filter_input(INPUT_POST, "prixD");
$prixR = filter_input(INPUT_POST, "prixR");
$imageP = filter_input(INPUT_POST, "imageP");
$image1 = filter_input(INPUT_POST, "image1");
$image2 = filter_input(INPUT_POST, "image2");
$image3 = filter_input(INPUT_POST, "image3");
$image4 = filter_input(INPUT_POST, "image4");
$image5 = filter_input(INPUT_POST, "image5");
$image6 = filter_input(INPUT_POST, "image6");
$image7 = filter_input(INPUT_POST, "image7");
$image8 = filter_input(INPUT_POST, "image8");
$image9 = filter_input(INPUT_POST, "image9");
$id_lots = filter_input(INPUT_POST, "id_lots");
$contact_proprio=filter_input(INPUT_POST,"contact_proprio");
$prenom_proprio=filter_input(INPUT_POST,"prenom_proprio");
$nom_proprio=filter_input(INPUT_POST,"nom_proprio");

$id_lots = preg_replace('~\D~', '', $id_lots);



$token=filter_input(INPUT_POST,"token");
if($token!=$_SESSION["token"]){
    ?>
    <script type="text/javascript">
        window.location.replace("error/oopsi.php");
    </script>
    <?php
    die;
}

require_once "../config.php";
$pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $pdo->prepare("insert into produit (titre,description,prixD,prixR,imageP,image1,image2,image3,image4,image5,image6,image7,image8,image9,id_lots, nom_proprio, prenom_proprio, contact_proprio) values (:titre,:description,:prixD,:prixR,:imageP,:image1,:image2,:image3,:image4,:image5,:image6,:image7,:image8,:image9,:id_lots, :nom_proprio, :prenom_proprio, :contact_proprio)");
$requete->bindParam(":titre", $titre);
$requete->bindParam(":description", $description);
$requete->bindParam(":prixD", $prixD);
$requete->bindParam(":prixR", $prixR);
$requete->bindParam(":imageP", $imageP);
$requete->bindParam(":image1", $image1);
$requete->bindParam(":image2", $image2);
$requete->bindParam(":image3", $image3);
$requete->bindParam(":image4", $image4);
$requete->bindParam(":image5", $image5);
$requete->bindParam(":image6", $image6);
$requete->bindParam(":image7", $image7);
$requete->bindParam(":image8", $image8);
$requete->bindParam(":image9", $image9);
$requete->bindParam(":id_lots", $id_lots);
$requete->bindParam(":nom_proprio", $nom_proprio);
$requete->bindParam(":prenom_proprio", $prenom_proprio);
$requete->bindParam(":contact_proprio", $contact_proprio);
$requete->execute();


$requete = $pdo->prepare("SELECT id FROM produit ORDER BY `id` DESC");
$requete->execute();



$lignes = $requete->fetchAll();


$id_produit123 = $lignes[0];
$id_produit = $id_produit123["id"];




$requete = $pdo->prepare("INSERT INTO propositions(id_produit, montant) values (:id_produit, :montant)");
$requete->bindParam(":id_produit", $id_produit);
$requete->bindParam(":montant", $prixD);
$requete->execute();




header("location:../admin/index.php");