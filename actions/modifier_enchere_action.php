<?php
session_start();
$id=filter_input(INPUT_POST,"id");
$titre_en=filter_input(INPUT_POST,"titre_en");
$description_en=filter_input(INPUT_POST,"description_en");
$dateDEB=filter_input(INPUT_POST,"dateDEB");
$dateEXP=filter_input(INPUT_POST,"dateEXP");
$etat_enchere=filter_input(INPUT_POST,"etat_enchere");



$token=filter_input(INPUT_POST,"token");


if ($token!=$_SESSION["token"]){
    ?>
    <script type="text/javascript">
        window.location.replace("error/oopsi.php");
    </script>
    <?php
    die;
}

require_once "../config.php";
$pdo = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BDD,Config::UTILISATEUR,Config::MOTDEPASSE);

$requete = $pdo->prepare("update encheres set titre_en=:titre_en, description_en=:description_en, dateDEB=:dateDEB, dateEXP=:dateEXP, etat_enchere=:etat_enchere where id=:id");
$requete->bindParam(":id",$id );
$requete->bindParam(":titre_en",$titre_en );
$requete->bindParam(":description_en",$description_en );
$requete->bindParam(":dateDEB",$dateDEB );
$requete->bindParam(":dateEXP",$dateEXP );
$requete->bindParam(":etat_enchere",$etat_enchere );

$requete->execute();

header("location:../encheres.php");