<?php
session_start();

$titre = filter_input(INPUT_POST, "titre_en");
$dateDEB = filter_input(INPUT_POST, "dateDEB");
$dateEXP = filter_input(INPUT_POST, "dateEXP");
$description = filter_input(INPUT_POST, "description_en");
$expiration = strtotime($dateEXP);
$debut = strtotime($dateDEB);
$maintenant = strtotime(date("Y-m-d H:i:s"));
if ($expiration<=$maintenant){
    $etat_enchere = "passé";
}
elseif ($debut<=$maintenant){
    $etat_enchere = "présent";
}
elseif ($debut>=$maintenant){
    $etat_enchere = "futur";
}
else {
    $etat_enchere = "erreur";
}

$token=filter_input(INPUT_POST,"token");
if($token!=$_SESSION["token"]){
    ?>
    <script type="text/javascript">
        window.location.replace("error/oopsi.php");
    </script>
    <?php
    die;
}

require_once "../config.php";
$pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $pdo->prepare("insert into encheres (dateDEB,dateEXP,titre_en,description_en,etat_enchere) values (:dateDEB,:dateEXP,:titre,:description,:etat_enchere)");
$requete->bindParam(":dateDEB", $dateDEB);
$requete->bindParam(":dateEXP", $dateEXP);
$requete->bindParam(":titre", $titre);
$requete->bindParam(":description", $description);
$requete->bindParam(":etat_enchere", $etat_enchere);
$requete->execute();

header("location:../admin/index.php");
