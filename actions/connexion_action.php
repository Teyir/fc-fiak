<?php
session_start();

$pseudo = filter_input(INPUT_POST, "pseudo");
//$mot_de_passe = hash('sha256',filter_input(INPUT_POST, "mot_de_passe"));
$mot_de_passe = filter_input(INPUT_POST, "mot_de_passe");

$hash = sha1($mot_de_passe);

$token = filter_input(INPUT_POST, "token");
if ($token != $_SESSION["token"]) {
    ?>
    <script type="text/javascript">
        window.location.replace("error/oopsi.php");
    </script>
    <?php
    die;
}


require_once "../config.php";
$pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $pdo->prepare("SELECT id, pseudo, groupe FROM users WHERE (pseudo=:pseudo) AND (mot_de_passe=:mot_de_passe)");
$requete->bindParam(":pseudo", $pseudo);
$requete->bindParam(":mot_de_passe", $hash);

$requete->execute();

$lignes = $requete->fetchAll();

if (count($lignes)==0){
    $_SESSION["erreur"]= 1;
    header("location:../connexion.php");
}
else {
    $_SESSION["id_connecté"]= htmlspecialchars($lignes[0]["id"]);
    $_SESSION["connecté"]= 1;
    $_SESSION["erreur"]= 0;
    if (htmlspecialchars($lignes[0]["groupe"])=="membre"){
        $_SESSION["admin"]= 0;
    }
    else {
        $_SESSION["admin"]= 1;
    }
    header("location:../index.php");
}