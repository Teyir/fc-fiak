<?php
//pour utiliser les sessions
session_start();

//recuperation données en POST
$pseudo=filter_input(INPUT_POST, "pseudo");
$nom=filter_input(INPUT_POST, "nom");
$prenom=filter_input(INPUT_POST, "prenom");
$adresse=filter_input(INPUT_POST, "adresse");
$codeP=filter_input(INPUT_POST, "codeP");
$pays=filter_input(INPUT_POST, "pays");
$email=filter_input(INPUT_POST, "email");
$mot_de_passe=filter_input(INPUT_POST, "mot_de_passe");

$hash = sha1($mot_de_passe);



$groupe="membre";

//recupreration de l'ip
if (!empty($_SERVER['HTTP_CLIENT_IP'])){
    $ip=$_SERVER['HTTP_CLIENT_IP'];
}elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
    $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
}else{
    $ip=$_SERVER['REMOTE_ADDR'];
}

$token=filter_input(INPUT_POST,"token");
if($token!=$_SESSION["token"]){
    ?>
    <script type="text/javascript">
        window.location.replace("error/oopsi.php");
    </script>
    <?php
    die;
}


require_once "../config.php";
$pdo = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $pdo->prepare("insert into users (pseudo, nom, prenom, adresse, codeP, pays, email, mot_de_passe, groupe, ip) values (:pseudo, :nom, :prenom, :adresse, :codeP, :pays, :email, :mot_de_passe, :groupe, :ip)");
$requete->bindParam(":pseudo", $pseudo);
$requete->bindParam(":nom", $nom);
$requete->bindParam(":prenom", $prenom);
$requete->bindParam(":adresse", $adresse);
$requete->bindParam(":codeP", $codeP);
$requete->bindParam(":pays", $pays);
$requete->bindParam(":email", $email);
$requete->bindParam(":mot_de_passe", $hash);
$requete->bindParam(":groupe", $groupe);
$requete->bindParam(":ip", $ip);

$requete->execute();

header("location:../connexion.php");
