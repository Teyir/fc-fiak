<?php
session_start();
$id=filter_input(INPUT_POST,"id");
$titre=filter_input(INPUT_POST,"titre");
$description=filter_input(INPUT_POST,"description");
$prixD=filter_input(INPUT_POST,"prixD");
$prixR=filter_input(INPUT_POST,"prixR");
$imageP=filter_input(INPUT_POST,"imageP");
$image1=filter_input(INPUT_POST,"image1");
$image2=filter_input(INPUT_POST,"image2");
$image3=filter_input(INPUT_POST,"image3");
$image4=filter_input(INPUT_POST,"image4");
$image5=filter_input(INPUT_POST,"image5");
$image6=filter_input(INPUT_POST,"image6");
$image7=filter_input(INPUT_POST,"image7");
$image8=filter_input(INPUT_POST,"image8");
$image9=filter_input(INPUT_POST,"image9");
$id_lots=filter_input(INPUT_POST,"id_lots");
$id_gagnant=filter_input(INPUT_POST,"id_gagnant");
$contact_proprio=filter_input(INPUT_POST,"contact_proprio");
$prenom_proprio=filter_input(INPUT_POST,"prenom_proprio");
$nom_proprio=filter_input(INPUT_POST,"nom_proprio");

$id_lots = preg_replace('~\D~', '', $id_lots);
$id_gagnant = preg_replace('~\D~', '', $id_gagnant);

$token=filter_input(INPUT_POST,"token");
if ($token!=$_SESSION["token"]){
    ?>
    <script type="text/javascript">
        window.location.replace("error/oopsi.php");
    </script>
    <?php
    die;
}

require_once "../config.php";
$pdo = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BDD,Config::UTILISATEUR,Config::MOTDEPASSE);

if ($id_gagnant==NULL){
    $requete = $pdo->prepare("update produit set titre=:titre, description=:description, prixD=:prixD, prixR=:prixR, imageP=:imageP, image1=:image1,image2=:image2,image3=:image3,image4=:image4,image5=:image5,image6=:image6,image7=:image7,image8=:image8,image9=:image9, id_lots=:id_lots, nom_proprio=:nom_proprio, prenom_proprio=:prenom_proprio, contact_proprio=:contact_proprio where id=:id");
    $requete->bindParam(":id",$id );
    $requete->bindParam(":titre",$titre );
    $requete->bindParam(":description",$description );
    $requete->bindParam(":prixD",$prixD );
    $requete->bindParam(":prixR",$prixR );
    $requete->bindParam(":imageP",$imageP );
    $requete->bindParam(":image1",$image1 );
    $requete->bindParam(":image2", $image2);
    $requete->bindParam(":image3", $image3);
    $requete->bindParam(":image4", $image4);
    $requete->bindParam(":image5", $image5);
    $requete->bindParam(":image6", $image6);
    $requete->bindParam(":image7", $image7);
    $requete->bindParam(":image8", $image8);
    $requete->bindParam(":image9", $image9);
    $requete->bindParam(":id_lots",$id_lots );
    $requete->bindParam(":nom_proprio", $nom_proprio);
    $requete->bindParam(":prenom_proprio", $prenom_proprio);
    $requete->bindParam(":contact_proprio", $contact_proprio);
}
else{
    $requete = $pdo->prepare("update produit set titre=:titre, description=:description, prixD=:prixD, prixR=:prixR, imageP=:imageP, image1=:image1,image2=:image2,image3=:image3,image4=:image4,image5=:image5,image6=:image6,image7=:image7,image8=:image8,image9=:image9, id_lots=:id_lots, id_gagnant=:id_gagnant, nom_proprio=:nom_proprio, prenom_proprio=:prenom_proprio, contact_proprio=:contact_proprio where id=:id");
    $requete->bindParam(":id",$id );
    $requete->bindParam(":titre",$titre );
    $requete->bindParam(":description",$description );
    $requete->bindParam(":prixD",$prixD );
    $requete->bindParam(":prixR",$prixR );
    $requete->bindParam(":imageP",$imageP );
    $requete->bindParam(":image1",$image1 );
    $requete->bindParam(":image2", $image2);
    $requete->bindParam(":image3", $image3);
    $requete->bindParam(":image4", $image4);
    $requete->bindParam(":image5", $image5);
    $requete->bindParam(":image6", $image6);
    $requete->bindParam(":image7", $image7);
    $requete->bindParam(":image8", $image8);
    $requete->bindParam(":image9", $image9);
    $requete->bindParam(":id_lots",$id_lots );
    $requete->bindParam(":id_gagnant",$id_gagnant );
    $requete->bindParam(":nom_proprio", $nom_proprio);
    $requete->bindParam(":prenom_proprio", $prenom_proprio);
    $requete->bindParam(":contact_proprio", $contact_proprio);
}


$requete->execute();
$requete->debugDumpParams();


header("location:../encheres.php");
