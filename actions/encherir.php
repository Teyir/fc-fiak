<?php
session_start();

$montant = filter_input(INPUT_GET, "mise");
$id_pseudo = filter_input(INPUT_GET, "pseudo");
$id_produit = filter_input(INPUT_GET, "id_produit");

$token=filter_input(INPUT_GET,"token");



if($token!=$_SESSION["token"]){
    ?>
    <script type="text/javascript">
        window.location.replace("error/oopsi.php");
    </script>
    <?php
    die;
}




require_once "../config.php";
$pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);



$requete = $pdo->prepare("select encheres.id, dateDEB, dateEXP from produit join lots on produit.id_lots=lots.id join encheres on lots.id_encheres=encheres.id where produit.id=:id");
$requete->bindParam(":id", $id_produit);
$requete->execute();

$fsqf = $requete->fetchAll();

$dateDEB = $fsqf[0]["dateDEB"];
$dateEXP = $fsqf[0]["dateEXP"];

$dateDEB = strtotime($dateDEB);
$dateEXP = strtotime($dateEXP);
$date = strtotime(date("Y-m-d H:i:s"));

if ($dateEXP >= $date )
{

    $requete = $pdo->prepare("INSERT INTO propositions(id_produit, id_users, montant) values (:id_produit, :id_users, :montant)");
    $requete->bindParam(":id_produit", $id_produit);
    $requete->bindParam(":id_users", $id_pseudo);
    $requete->bindParam(":montant", $montant);
    $requete->execute();

    header("location: ../encherir.php?id=".$id_produit);

}else
{
    echo "Enchère terminée";

    sleep(5);

   echo '<a href="../encheres.php">Enchère terminée, revenir aux enchères</a>';
}








