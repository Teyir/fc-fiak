<?php
session_start();
$id=filter_input(INPUT_POST,"id");
$avatar=filter_input(INPUT_POST,"avatar");

$token=filter_input(INPUT_POST,"token");
if ($token!=$_SESSION["token"]){
    ?>
    <script type="text/javascript">
        window.location.replace("error/oopsi.php");
    </script>
    <?php
    die;
}

require_once "../config.php";

$pdo = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BDD,Config::UTILISATEUR,Config::MOTDEPASSE);

$requete = $pdo->prepare("update users set avatar=:avatar where id=:id");
$requete->bindParam(":id",$id );
$requete->bindParam(":avatar",$avatar );

$requete->execute();


header("location:../profil.php");
