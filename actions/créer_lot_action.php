<?php
session_start();

$titre_lots = filter_input(INPUT_POST, "titre_lots");
$id_encheres = filter_input(INPUT_POST, "id_encheres");
$description_lots = filter_input(INPUT_POST, "description_lots");


$id_encheres = preg_replace('~\D~', '', $id_encheres);


$token=filter_input(INPUT_POST,"token");
if($token!=$_SESSION["token"]){
    ?>
    <script type="text/javascript">
        window.location.replace("error/oopsi.php");
    </script>
    <?php
    die;
}

require_once "../config.php";
$pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $pdo->prepare("insert into lots (titre_lots,description_lots,id_encheres) values (:titre_lots,:description_lots,:id_encheres)");
$requete->bindParam(":id_encheres", $id_encheres);
$requete->bindParam(":titre_lots", $titre_lots);
$requete->bindParam(":description_lots", $description_lots);
$requete->execute();

header("location:../admin/index.php");
