<?php
require_once "includes/head.php";

title_head("BitGameCoin");
$id=filter_input(INPUT_GET,"id");
$id=filter_input(INPUT_GET,"id");

require_once "config.php";
$pdo = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BDD,Config::UTILISATEUR,Config::MOTDEPASSE);

$requete = $pdo->prepare("select id, titre_lots, description_lots from lots where id_encheres=:id");
$requete->bindParam(":id",$id);
$requete->execute();

$lignes= $requete->fetchAll();


if (count($lignes)==0){
    echo "pas de lots pour cette enchère";
    die;
}

require_once "functions/desc.php";

?>
    <div class="row">
        <?php
        for($i = 0;$i< count($lignes);$i++){
            $description = $lignes[$i]["description_lots"];

            $description = desc($description, 240)

            ?>
            <div class="wrapper">
                <div class="cols">
                    <div class="col" ontouchstart="this.classList.toggle('hover');">
                        <div class="container">
                            <div class="front">
                                <div class="inner">
                                    <p><?php echo htmlspecialchars($lignes[$i]["titre_lots"]) ?></p>
                                    <span><?php echo $description ?></span>
                                </div>
                            </div>
                            <div class="back">
                                <div class="inner">
                                    <a href="produits.php?id=<?php echo htmlspecialchars($lignes[$i]["id"]) ?>" class="btn btn-sm btn-primary"> Voir les produits </a>
                                    <?php if (isset($_SESSION['admin'])==TRUE){ ?>
                                        <?php if ($_SESSION["admin"]==1){ ?>
                                            <a href="admin/modifier_lot.php?id=<?php echo htmlspecialchars($lignes[$i]["id"]) ?>" class="btn btn-sm btn-warning"> modifier </a>
                                        <?php } ?>
                                        <?php if ($_SESSION["admin"]==1){ ?>
                                            <a href="admin/supprimer_lot.php?id=<?php echo htmlspecialchars($lignes[$i]["id"]) ?>" class="btn btn-sm btn-danger"> Supprimer </a>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>



            <?php
        }
        ?>
    </div>


<?php
require_once "includes/footer.php";
