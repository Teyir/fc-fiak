<?php
require_once "includes/head.php";
require_once "entete.php";
require_once "includes/stats_accueil.php";

title_head("Accueil | BitGameCoin");

?>

<body>
<h3 class="desc_accueil text-center">Découvrez qui nous sommes... </h3>



    <div class="stats_accueil">
        <div class="row">
            <div class="col-md-3 col-sm-6 text-center"> <i class="fas fa-users medium-icon"></i> <span class="statistic-counter" data-aos="zoom-in"><?php echo $nb_membres ?></span>
                <p class="counter-title" >Membres inscrits</p>
            </div>

            <div class="col-md-3 col-sm-6 text-center"> <i class="fas fa-trophy medium-icon"></i> <span class="statistic-counter" data-aos="zoom-in"><?php echo $nb_encheres_finish ?></span>
                <span class="counter-title">Enchères terminées</span>
            </div>

            <div class="col-md-3 col-sm-6 text-center"> <i class="fas fa-ticket medium-icon"></i> <span class="statistic-counter" data-aos="zoom-in"><?php echo $nb_encheres_on  ?></span>
                <span class="counter-title">Enchères en cours</span>
            </div>

            <div class="col-md-3 col-sm-6 text-center"> <i class="fas fa-gifts medium-icon"></i> <span class="statistic-counter" data-aos="zoom-in"><?php echo $nb_produits ?></span>
                <span class="counter-title">Produits en vente</span>
            </div
        </div>
    </div>



<?php
require_once "includes/footer.php";
?>
