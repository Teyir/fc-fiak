<?php
require_once "includes/head.php";
title_head("image du Profil");

$iduser=$_SESSION["id_connecté"];

$pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $pdo->prepare("select avatar, id from users");
$requete->bindParam(":id",$iduser );
$requete->bindParam(":avatar",$avatar );
$requete->execute();
$donnees=$requete->fetchAll();

if (count($donnees)==0){
    http_response_code(404);
    ?>
    <script type="text/javascript">
        window.location.replace("error/oopsi.php");
    </script>
    <?php
    die;
}
$ligne=$donnees[0];

$token=rand(0,100000);
$_SESSION["token"]=$token;
?>


<div class="container text-center">
    <h3>Modifier votre avatar</h3>
    <form action="actions/modifierImgProfil.php" method="post">
        <input type="hidden" value="<?php echo $iduser ?>" name="id" id="id">
        <input type="hidden" value="<?php echo $token ?>" name="token" id="token">
        <div class="form-group">
            <label for="avatar">URL de l'avatar</label>
            <input type="url" class="form-control" value="<?php echo $donnees[0]["avatar"]?>"  id="avatar" name="avatar" placeholder="Saisissez l'url de votre image" required>
        </div>
        <input type="submit" class="btn btn-primary">
    </form>
</div>



<?php
    require_once "includes/footer.php";
?>