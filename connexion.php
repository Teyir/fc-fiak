<?php

require_once "includes/head.php";



title_head("Connexion | FC-Fiak.fr");


$token=rand(0, 1000000);

$_SESSION["token"]=$token;

?>
<?php
if (isset($_SESSION["erreur"]) == TRUE) {
    if ($_SESSION["erreur"] == 1){
        echo ("la connexion a échoué");
    }
}


?>
    <div class="container register">
        <div class="row">
            <div class="col-md-3 register-left">
                <img src="img/logo_no_letters.png" alt="Logo FC-Fiak">
                <h3>Bienvenue</h3>
                <p>Vous êtes sur le point de vous connecter sur le site FC-Fiak.fr!</p>
                <a class="btn btn-light" href="inscription.php" role="button">Inscription</a>
                <br>
                <br>
            </div>

            <div class="col-md-9 register-right">
                <form action="actions/connexion_action.php" method="post">
                    <input type="hidden" value="<?php echo $token ?>" name="token">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <h3 class="register-heading">Connexion</h3>
                            <div class="row register-form">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Votre pseudo *" id="pseudo" name="pseudo" maxlength="50" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" placeholder="Mot de passe *" id="mot_de_passe" name="mot_de_passe" maxlength="50" required>
                                    </div>

                                    <input type="submit" class="btnRegister"  value="Se connecter">
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </div>

    </div>

<?php
require_once "includes/footer.php";
