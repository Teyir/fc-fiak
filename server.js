var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use("/", express.static(__dirname + "/chat"));

io.on('connection', function (socket) {

  var loggedUser;


  console.log('Un utilisateur vient de se connecter');


  socket.on('disconnect', function () {
    if (loggedUser !== undefined) {
      console.log(loggedUser.username + ' vient de se deconnecter');
      var serviceMessage = {
        text: 'Utilisateur "' + loggedUser.username + '" vient de se deconnecter',
        type: 'logout'
      };
      socket.broadcast.emit('service-message', serviceMessage);
    }
  });

  socket.on('user-login', function (user) {
    loggedUser = user;
    if (loggedUser !== undefined) {
      var serviceMessage = {
        text: 'Utilisateur "' + loggedUser.username + '" vient de se connecter',
        type: 'login'
      };
      socket.broadcast.emit('service-message', serviceMessage);
    }
  });

  socket.on('chat-message', function (message) {
    message.username = loggedUser.username;
    io.emit('chat-message', message);
    console.log('Message de : ' + loggedUser.username);
  });
});


http.listen(3000, function () {
  console.log('Écoute du serveur sur le port *:3000');
});
