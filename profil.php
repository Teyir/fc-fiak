<?php
require_once "includes/head.php";



title_head("Profil");

$iduser=$_SESSION["id_connecté"];

?>
<?php


$pdo = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete123 = $pdo->prepare("select nom, prenom, date_INS, pseudo, adresse, codeP, pays, email, groupe, avatar from users where id=:id");
$requete123->bindParam(":id", $iduser);
$requete123->execute();
$profil = $requete123->fetchAll();

?>

<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card2">

                <div class="card-body2">

                    <div class="col-lg-4 col-sm-12 col-md-12">
                        <form method="post" action="actions/modifierImgProfil.php" role="form" enctype="multipart/form-data">

                            <label for="img_profil">

                                <div class="card-container">
                                    <input type="file" class="form-control-file d-none" name="img_profil" id="img_profil"
                                           onclick="window.location.href='modifier_photo_profil.php'" ' required />
                                    <img class="profile-image" src="<?php echo htmlspecialchars($profil[0]['avatar']) ?>">

                                    <div class="hoverText">
                                        <div class="caption">
                                            <i class="fas fa-edit"></i>
                                        </div>
                                    </div>
                                </div>

                            </label>


                            <div class="form-group">



                            </div>

                        </form>
                    </div>




                    <div class="row">
                        <div class="col-12">
                            <div class="tab-content ml-1" id="myTabContent">
                                <div class="tab-pane fade show active" id="basicInfo" role="tabpanel" aria-labelledby="basicInfo-tab">


                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label class="champ">Nom</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php echo htmlspecialchars($profil[0]["nom"]) ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label class="champ">Prénom</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php echo htmlspecialchars($profil[0]["prenom"]) ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label class="champ">Date d'inscription</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php echo htmlspecialchars($profil[0]["date_INS"]) ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label class="champ">pseudo</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php echo htmlspecialchars($profil[0]["pseudo"]) ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label class="champ">adresse</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php echo htmlspecialchars($profil[0]["adresse"]) ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label class="champ">code postal</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php echo htmlspecialchars($profil[0]["codeP"]) ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label class="champ">pays</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php echo htmlspecialchars($profil[0]["pays"]) ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label class="champ">email</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php echo htmlspecialchars($profil[0]["email"]) ?>
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label class="champ">groupe</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            <?php echo htmlspecialchars($profil[0]["groupe"]) ?>
                                        </div>
                                    </div>
                                    <hr />

                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>
</div>

<?php
require_once "includes/footer.php";