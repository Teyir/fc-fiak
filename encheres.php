<?php
require_once "includes/head.php";


title_head("BitGameCoin");

require_once "config.php";
$pdo = new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BDD,Config::UTILISATEUR,Config::MOTDEPASSE);
$requete = $pdo->prepare("select id, dateDEB, dateEXP, titre_en, description_en, etat_enchere from encheres");
$requete->execute();
$lignes = $requete-> fetchAll();


require_once "functions/desc.php";

?>

<div class="row">
    <?php
    for($i = 0;$i< count($lignes);$i++){
        $description = $lignes[$i]["description_en"];

        $description = desc($description, 240)

        ?>
    <div class="wrapper">
        <div class="cols">
            <div class="col" ontouchstart="this.classList.toggle('hover');">
                <div class="container">
                    <div class="front">
                        <div class="inner">
                            <p><?php echo htmlspecialchars($lignes[$i]["titre_en"]) ?></p>
                            <span>État de l'enchère :<strong> <?php echo htmlspecialchars($lignes[$i]["etat_enchere"]) ?></strong></span><br>
                            <span>Date de début : <?php echo htmlspecialchars($lignes[$i]["dateDEB"]) ?></span><br>
                            <span>Date d'expiration : <?php echo htmlspecialchars($lignes[$i]["dateEXP"]) ?></span><br>
                        </div>
                    </div>
                    <div class="back">
                        <div class="inner">
                            <p><?php echo $description; ?></p>
                            <a href="lots.php?id=<?php echo htmlspecialchars($lignes[$i]["id"]) ?>" class="btn btn-sm btn-primary"> Voir les lots </a>
                            <?php if (isset($_SESSION['admin'])==TRUE){ ?>
                                <?php if ($_SESSION["admin"]==1){ ?>
                                    <a href="admin/modifier_enchere.php?id=<?php echo htmlspecialchars($lignes[$i]["id"]) ?>" class="btn btn-sm btn-warning"> modifier </a>
                                <?php } ?>
                                <?php if ($_SESSION["admin"]==1){ ?>
                                    <a href="admin/supprimer_enchere.php?id=<?php echo htmlspecialchars($lignes[$i]["id"]) ?>" class="btn btn-sm btn-danger"> Supprimer </a>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

        <?php
    }
    ?>

</div>
<?php
require_once "includes/footer.php";