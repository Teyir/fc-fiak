<?php
session_start();

?>
<header>
    <div class="header-content" >
        <div class="container-fluid col-9">
        <?php
    function navbar()
   {
 ?>

            <nav class="navbar navbar-expand-lg navbar-dark bg-transparent">

                <button class="navbar-toggler navbar-toggler-right ml-auto" type="button" data-toggle="collapse" data-target="#navbarMain" aria-controls="navbarMain" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarMain">

                            <li class="nav-item ">
                                <a href="index.php" class="nav-link">Accueil</a>
                            </li>

                            <li class="nav-item">
                                <a href="encheres.php" class="nav-link">Enchères</a>
                            </li>

                            <li class="nav-item">
                                <a href="inscription.php" class="nav-link">Inscription / Connexion</a>
                            </li>

                </div>
            </nav>
</header>

                <?php
}

function navbar_connecte($pseudo){

$iduser=$_SESSION["id_connecté"];

require_once "config.php";

$pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

$requete = $pdo->prepare("select avatar, id from users where id=:id");
$requete->bindParam(":id",$iduser );
$requete->execute();
$pp = $requete->fetchAll();

    ?>

            <nav class="navbar navbar-expand-lg navbar-dark bg-transparent">

                <button class="navbar-toggler navbar-toggler-right ml-auto" type="button" data-toggle="collapse" data-target="#navbarMain" aria-controls="navbarMain" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarMain">

                            <li class="nav-item">
                                <a href="index.php" class="nav-link">Accueil</a>
                            </li>

                            <li class="nav-item">
                                <a href="encheres.php" class="nav-link">Enchères</a>
                            </li>

                            <li class="nav-item">
                                <a href="live_support.php" class="nav-link">Live support</a>
                            </li>


                            <li class="nav-item dropdown ml-auto">

                                <a id="profil" class="nav-link dropdown-toggle btn " href="#" id="dropdown-tools" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="<?php echo $pp[0]["avatar"]?>" style="margin-left: -10px; width: 24px; height: 24px"> <?php echo $pseudo ?>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="<?php echo $pseudo ?>">


                                    <!-- Administration -->
                                    <?php if ($_SESSION["admin"] == 1) { ?>
                                        <a href="admin/index.php" class="dropdown-item text-success"><i class="fas fa-tachometer-alt"></i> Administration</a>
                                        <div class="dropdown-divider"></div>
                                    <?php } ?>


                                    <a class="dropdown-item" href="profil.php"><i class="fas fa-user"></i> Mon profil</a>

                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item text-danger" href="actions/deconnexion_action.php"><i class="fas fa-sign-out-alt"></i> Se déconnecter</a>

                                </div>
                            </li>

            </div>
        </nav>


                    <!-- Logo + description -->
                    <div class="main-header-text">
                        <div class="text-center header-txt">
                            <img class="img_header" src="img/logo.png">

                            <h1 class="header_titre">BitGame Coin</h1>
                            <h4 class="header_description">Le site n°1 de ventes aux enchères à l'aveugle !</h4>

                        </div>
                    </div>

            </div>
            </header>

    <?php
}
if (isset($_SESSION["connecté"]) == False) {
    navbar();
}
else if ($_SESSION["connecté"] == False) {
    navbar();
}
else if ($_SESSION["connecté"] == 1){

    $id=$_SESSION["id_connecté"];

    require_once "config.php";
    $pdo = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BDD, Config::UTILISATEUR, Config::MOTDEPASSE);

    $requete = $pdo->prepare("SELECT pseudo FROM users WHERE (id=:id)");
    $requete->bindParam(":id", $id);

    $requete->execute();

    $lignes = $requete->fetchAll();

    $pseudo = htmlspecialchars($lignes[0]["pseudo"]);

    navbar_connecte($pseudo);
}

